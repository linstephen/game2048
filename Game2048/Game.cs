﻿using System;
using System.Collections.Generic;

namespace Game2048
{
	internal class Game
	{
		private readonly Dictionary<ConsoleKey, Direction> _keyMapping;

		public Game()
		{
			GameBoard = new GameBoard(4, 4);
			Score = 0;

			_keyMapping = new Dictionary<ConsoleKey, Direction>
			{
				[ConsoleKey.UpArrow] = Direction.Up,
				[ConsoleKey.DownArrow] = Direction.Down,
				[ConsoleKey.LeftArrow] = Direction.Left,
				[ConsoleKey.RightArrow] = Direction.Right
			};

			Renderer = new GameRenderer(this);
		}

		public ulong Score { get; private set; }
		private GameRenderer Renderer { get; }
		public GameBoard GameBoard { get; }
		private Random Random { get; } = new Random();

		public void Run()
		{
			var hasUpdated = true;

			do
			{
				if (hasUpdated) GameBoard.PutNewValue(Random);

				Renderer.Display();

				if (IsDead())
				{
					PrintDeadMessage();
					break;
				}

				var input = GetInput();

				hasUpdated = _keyMapping.ContainsKey(input.Key) && Update(_keyMapping[input.Key]);
			} while (true); // use CTRL-C to break out of loop

			Console.WriteLine("Press any key to quit...");
			Console.Read();
		}

		private void PrintDeadMessage()
		{
			Renderer.PrintDeadMessage();
		}

		private static ConsoleKeyInfo GetInput()
		{
			Console.WriteLine("Use arrow keys to move the tiles. Press Ctrl-C to exit.");
			var input = Console.ReadKey(true); // BLOCKING TO WAIT FOR INPUT
			Console.WriteLine(input.Key.ToString());
			return input;
		}

		private bool Update(Direction dir)
		{
			var isUpdated = GameBoard.Update(dir, out var score);
			Score += score;
			return isUpdated;
		}

		private bool IsDead()
		{
			foreach (var dir in _keyMapping.Values)
			{
				var clone = GameBoard.Clone();
				if (clone.Update(dir, out _)) return false;
			}

			// tried all directions. none worked.
			return true;
		}
	}

	public enum Direction
	{
		Up,
		Down,
		Right,
		Left
	}
}